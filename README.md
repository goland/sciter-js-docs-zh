## Sciter-js文档翻译

[Sciter-js原文档](https://github.com/c-smile/sciter-js-sdk/tree/main/docs/md)

## 完成进度
### DOM
- [Node](./docs-zh/Node.md)
- [Element](./docs-zh/Element.md)
  - [Element.Style](./docs-zh/Element.Style.md)
  - [Element.State](./docs-zh/Element.State.md)
  - [Element.Selection](./docs-zh/Element.Selection.md)
- [Document](./docs-zh/Document.md)
- [Window](./docs-zh/Window.md)
- [HTML](./docs-zh/HTML.md)
- [Event](./docs-zh/Event.md)
- [Graphics](./docs-zh/Graphics/README.md)
- [Audio](./docs-zh/Audio.md)
- [Range](./docs-zh/Range.md)

### Reactor
- [Reactor](./docs-zh/reactor/README.md)
- [JSX](./docs-zh/reactor/JSX.md)
- [Render](./docs-zh/reactor/rendering.md)
- [Components](./docs-zh/reactor/components.md)
- [Components Data and Lifecycle](./docs-zh/reactor/component-update.md)
- [Styles, Events and Mounting points](./docs-zh/reactor/component-styles-events.md)
- [Lists and Keys](./docs-zh/reactor/lists-and-keys.md)
- [Reactor vs ReactJS](./docs-zh/reactor/reactor-vs-reactjs.md)

### CSS
-  [CSS](./docs-zh/css/README.md)
-  [css-sciter](./docs-zh/css/css-sciter.md)
-  [css-layout](./docs-zh/css/css-layout.md)
-  [css-render](./docs-zh/css/css-render.md)

### Behaviors
- [behavior](./docs-zh/behaviors/README.md)

### Clipboard
- [Clipboard](./docs-zh/Clipboard.md)
  
### Module
- [@env](./docs-zh/module-env.md)
- [@sciter](./docs-zh/module-sciter.md)
- [@sys](./docs-zh/module-sys.md)
  - [@sys.fs.watch](./docs-zh/sys.fs/watch.md)
  - [@sys.fs.splitpath](./docs-zh/sys.fs/splitpath.md)
- [@storage](./docs-en/storage/README.md)