# class Document

备注::Sciter.JS中`Document`继承[`Element`](Element.md)类 

- `document`表示文档根节点元素,索引
- `document.documentElement === document`.
- `document`可以使用`Element`的所有属性和方法
  
## 属性 Properties:

* `document.body`
* `document.head`
* `document.documentElement`

## 方法:

* `document.querySelector("selector")`
* `document.$("selector")` 等价于上面
* `document.querySelectorAll("selector")`
* `document.$$("selector")`等级于上面
* `document.getElementById("id")`
* `document.createElement()`
* `document.createTextNode()`
* `document.createComment()`
* `document.createDocumentFragment()`
* `document.createNodeIterator(root[, whatToShow[, filter]])`返回`NodeIterator`


## 方法 (Sciter特有):

* #### `document.bindImage(url:string[, img:Graphics.Image]) : Graphics.Image`
  管理css属性中的image属性 

JS:
```JavaScript
   document.bindImage("in-memory:dynback");
```
CSS:
```CSS
   div {
      /* uses image supplied by script: */
      background-image: url("in-memory:dynback"); 
   }
```
