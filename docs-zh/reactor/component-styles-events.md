## 组件的样式定义(Component-Style)

通常在全局定义样式,然而有时候组件需要特定的样式,

Reactor提供了`styleset`属性来实现自定义样式


```JavaScript
class Clock extends Element 
{

  time = new Date(); // setting initial state 

  componentDidMount() {
    this.timer(1000, function() {
      this.componentUpdate { time:new Date() };
      return true; // to keep the timer ticking
    });
  }

  render() {
    return <clock styleset={__DIR__ + "styles.css#clock"}>
      <div.greeting>Hello, world!</div>
      <div>It is {this.data.time.toLocaleTimeString()} now.</div>
     </clock>;
  } 
}
```

备注: 代码中的`styleset={__DIR__ + "styles.css#clock"}`将引用`styles.css`的`clock`部分.

```CSS
@set clock 
{
  :root {
    display: block;
    flow:vertical;
  }
  span.time { 
    display:inline-block; 
    white-space:nowrap; 
  }
}
```
Sciter的css[StyleSet](../css/README.md#StyleSet)


## 组件的事件处理

Sciter使用Class组件实现事件的回调处理
如下的搜索组件

```JavaScript
class Search extends Element {

  render() {
    return <search>
        <input|text />
        <button.do/>
    </search>;
  }

  ["on click at button.do"](evt, button) { 
    this.post(new Event("do-search", {data: this.$("input").value} )); 
  }
  ["on change at input"](evt,input) { 
    this.showSuggestionsFor( input.value ); 
  }

  get value() { return this.$("input").value; }
  set value(nv) { this.$("input").value = nv;

}
```
上面的代码注册了2个事件`button`的`click`事件和`input`的`change`事件


事件处理回调定义 `["on change at input"](evt,input)`是`JS/ES2020`的新的语法形式.
参考文档[computable names](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Method_definitions#computed_property_names)
Sciterjs使用这种结构实现事件的处理 其规则如下:    

```JavaScript
["on eventname"](event) {}
["on eventname at selector"](event, selectorElement) {}
```

Where:  

* **on**\[space\] 声明为事件处理函数;     
* *eventname*  表示处理的事件名称,对应标准的dom事件名称;     
* [space]**at**[space],表示子元素选择器;    
* *selector* css的选择器表达式,用来指定子元素.   

Sciter使用如上约定实现事件处理,不需要使用`addEventHandler`

## HTML Resident Mounting Points

Sciterjs支持在html中使用jsx表达式

ReactJS通常在js中使用jsx表达式,如下
```JavaScript
function App() {
  return
    <main>
       <Welcome name="Ivan" />
       <Welcome name="Olga" />
       <Welcome name="Andrew" />
    </main>;
}

document.body.patch( <App /> );
```

Sciter提供了`<reactor>`标签支持在HTML中使用JSX表达式
```XML
<body>
   <p>Test of Tabs component.</p>
      
   <reactor(Tabs) src="tabs.js">
     <tab(first) label="First tab">First tab content</tab>
     <tab(second) label="Second tab">Second tab content</tab>
     <tab(third) label="Third tab" src="tab-content.htm"></tab>
   </reactor>

</body>
```

<reactor>接受2个属性参数
* *name* - Class组件的名称
* *src* - 组件的定义文件.

上面的`reactor`将会解析为jsx表达式,可以将`<reactor>`看做一个`<scripter>`标签
```XML
<body>
   <p>Test of &lt;Tabs> component.</p>
      
   <script type="text/javascript+jsx" component="Tabs" src="tabs.tis">
     <tab(first) label="First tab">First tab content</tab>
     <tab(second) label="Second tab">Second tab content</tab>
     <tab(third) label="Third tab" src="tab-content.htm"></tab>
   </script>

</body>
```
不同的是`<reactor>`作为占位符,在解析后将被真实的组件内容所替换调,
渲染结果如下
```XML
<body>
   <p>Test of &lt;Tabs> component.</p>
      
   <tabs>
     <tab(first) label="First tab">First tab content</tab>
     <tab(second) label="Second tab">Second tab content</tab>
     <tab(third) label="Third tab" src="tab-content.htm"></tab>
   </tabs>

</body>
```
