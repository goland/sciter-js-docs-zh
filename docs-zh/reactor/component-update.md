
备注:这篇文档有意的与 [RectJS/State and Lifecycle](https://reactjs.org/docs/state-and-lifecycle.html)组织成相似的结构,以便ReactJS阅读理解,但是细节有所不同

## 数据模型(Data [Model])和组件的生命周期(Componment Lifecycle)

Sciter's Reactor中的组件(Components)可以看做`Model-View-Controller(MVC)`的实现

渲染一些数据`Data[Model]`为`View`,
用户操作`View`触发事件回调,
在事件回调中修改数据`Data`,
这种事件回调中修改数据组成一个`Controller`


### 组件类(Component Class)


每个组件类实现为包含一个`render()`方法
```JavaScript
class Clock extends Element {
  render() {
    return <div>
        <h1>Hello, world!</h1>
        <h2>It is {this.time.toLocaleTimeString()}.</h2>
      </div>;
  }
}
```
在每次更新周期中会自动调用组件类的`render()`
可以在组件类中实现状态管理`State`和生命周期的管理`lifecycle`

## 组件类中添加本地数据状态

```JavaScript
class Clock extends Element 
{
  // declaring local data storage with initial value 
  time = new Date(); 

  render() {
    return <div>
        <h1>Hello, world!</h1>
        <h2>It is {this.time.toLocaleTimeString()} now.</h2>
      </div>;
  } 
} 
```
上面的组件类中添加了本地数据`time`,
在`render()`方法中可以通过`this.time`调用本地数据
`Clock`类仅仅渲染一次,下面添加生命周期相关方法实现自动更新

## 组件类添加生命周期方法 Adding Lifecycle Methods to a Class

Sciter提供了`componentDidMount()`在挂载到元素后的回调,
类似`ReactJS`的生命周期回调函数
Sciterjs没有`Hook`的功能


```JavaScript
class Clock extends Element 
{
  time = new Date(); // setting initial state 

  componentDidMount() { // instance of the class is attached to real DOM
    this.timer(1000, () => {
      this.componentUpdate({ time:new Date() });
      return true; // to keep the timer ticking
    });
  }

  render() {
    return <div>
      <h1>Hello, world!</h1>
      <h2>It is {this.time.toLocaleTimeString()} now.</h2>
     </div>;
  } 
} 
```
在`componentDidMount()`中调用了`this.timer()`

```JavaScript
() => {
  this.componentUpdate({ time:new Date() });
  return true; // to keep the timer ticking
}
```
内容调用了`componentUpdate()`更新数据
Scitert提供了如下的生命周期函数

* `componentDidMount()` \- 挂载后调用
* `componentWillUnmount()` \- 删除后调用 . 
* `Element.timer()` 创建元素关联的定时器.     
* `componentUpdate(props)` - 更新组件内部数据.

## 更新组件状态 
`componentUpdate()`的功能类似如下:

```JavaScript
class Element
{
  componentUpdate(newdata = null) {
    if(typeof newdata == "object") 
      Object.assign(this,newdata);
    this.post(() => this.patch(this.render()));
  }
}
```

整体流程如下
1. `Object.assign(this,newdata);` - 更新组件的属性;
1. `{ this.patch(this.render()); }` - 实现渲染更新
   
   * 调用`this.render()`生成新的虚拟dom;
   * 调用 `Element.patch(velement)`更新旧的dom;
1. `this.post(updater)` - 在下个更新周期中调用回调函数,类似vue的this.$nextTick().
.

## 正确的使用数据
在使用`componentUpdate(newdata)`时需要注意以下3点

### 不要直接修改数据

```
// WRONG!
this.comment = "Hello";
```

而是使用 `componentUpdate()`:

```
// Correct
this.componentUpdate({comment: "Hello"});
```

### 数据更新的合并 Data Updates are Merged
多次调用`componentUpdate()`会进行合并
```JavaScript
clock.componentUpdate { time:new Date() };
...
clock.componentUpdate { greeting: "John" };
```

只会进行一次`clock.render()`调用
> SciterJS的代码中可以对单个参数省略括号:
  ```
  obj.method { name:val };
  ```
  是下面的缩写:
  ```
  obj.method({ name:val });
  ```

