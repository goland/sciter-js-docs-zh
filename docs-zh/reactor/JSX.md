
备注:这篇文档有意的与 [ReactJS/JSX introduction](https://reactjs.org/docs/introducing-jsx.html)组织成相似的结构,以便ReactJS阅读理解

## SciterJS::JSX间接 

考察下面的变量声明
```JavaScript
const velement = <h1 id="hw">Hello, world!</h1>;
```
如上表达式称作JSX,SciterJS内置JSX解析,不需要安装babel等jsx解析库
JSX既不是字符串(string)也不是HTML元素(dom),只是一个类似数组的声明,

```JavaScript
const velement = ["h1", { id:"hw"}, ["Hello, world!"] ];
```
也可以使用如上的数组声明代替JSX语法,效果是一样的,

## JSX内嵌表达式

在JSX中可以使用变量表达式引用已声明的变量,如下
```JavaScript
const name = "Alice";
const velement = <h1>Hello, {name}</h1>;
```

类似的js数组语法如下
```JavaScript
const velement = ["h1", {}, [name] ];
```

可以在`{}`中使用任何有效的js表达式
```JavaScript
const velement = <div>1 + 1 is { 1 + 1 }</div>;
```

## JSX作为表达式使用

JSX也可以作为表达式使用,如下，
```JavaScript
function getGreeting(user) {
  if (user)
    return <h1>Hello, {formatName(user)}!</h1>;
  else
    return <h1>Hello, Stranger.</h1>;
}
```
上面的函数根据`user`变量返回不同的JSX表达式,

## JSX中设置属性

也可以在JSX中指定标签的属性
```JavaScript
const velement = <div tabindex="0"></div>;
```

标签的属性也可以是js变量的值
```JavaScript
const velement = <img src={user.avatarUrl}></img>;
```

备注:标签属性的值,`""`使用普通的字符串值,`{}`使用js表达式的值,不可以同时使用`""`和`{}`

## JSX中设置子元素

可以使用`/>`表示空元素节点
```JavaScript
const velement = <img src={user.avatarUrl} />;
```
.
备注:JSX中`<img>`, `<input>` 和 `<br>`不能使用闭合标签,只能使用单标签形式 `<img />`, `<input />` or `<br />`

JSX也可以嵌套包含子元素:
```JavaScript
const velement = 
  <div> 
    <h1>Hello!</h1>
    <h2>Good to see you here.</h2> 
  </div>;
```

## JSX中设置运行时状态

可以在JSX中声明状态信息,如下:
```JavaScript
const velement = 
  <li state-expanded={ isOpen(item) } > 
    <caption>Hello!</caption>
    <div></div> 
  </div>;
```

其效果类似于CSS中的`:active`, `:hover`, `:checked`

### JSX设置input标签的值

JSX中使用`state-value`设置`<input>`标签的值，

```JavaScript
<input|text(firstname) state-value="John" />
```

备注`value`属性设置的是初始值`<input|text(firstname) value="Initial value" />`

### JSX设置html内容

可以使用`state-html`属性设置html的内容,如下
```JavaScript
const htmlContent = "<b>some</b> literal <i>HTML</i>";
<div state-html={Content} />
```

运行结果如下:
```JavaScript
<div>
  <b>some</b> literal <i>HTML</i>
</div>
```

## Sciter's的HTML缩写
JSX支持SciterJS的HTML缩写形式
* `<input(firstName) />` => `<input name="firstName" />`
* `<input|text />` => `<input type="text" />`
* `<input.search />` => `<input class="search" />`
* `<input#lookup />` => `<input id="lookup" />`

## JSX的本质是一个函数  


```JavaScript
const velement = <h1 id="hw">Hello, world!</h1>;
```
在解析后生成内置的JSX函数调用

```JavaScript
const velement = JSX("h1", {id:"hw"}, ["Hello, world!"]);
```
默认的JSX函数实现如下

```JavaScript
function JSX(tagName,attributes,children) {
  return [tagName,attributes,children];
}
```
所以JSX表达式的结果就是一个数组声明,
这种结构的数组称作`VNODES`,也就是虚拟dom(virtual DOM node)

```JavaScript
["tag", {attributes}, [children] ]
```



* *tag* - 表示标签的名称,比如: `div`, `p`, `section`.
* *attributes* - 表示标签的属性(k,v)组成的数对象,`attributes`可以是`null`或者一个普通对象(object);
* *children* -  表示标签的子节点组成的属性,子节点可以是字符串或者vnodes,`children`可以是`null`或者一个数组(array) ;   

> 备注: **JSX函数可以在js中重新定义**. 
> 比如在使用js库`MithrilJS`,可以将JSX定义为:
  ```JavaScript
  JSX = m; // m is a Mithril's function - constructor of vnodes
  ```

## VNODE的使用和渲染

`element`的一些方法支持jsx作为参数来操作dom
```JavaScript
container.content(<div>Hello wonderful world</div>);
```


```JavaScript
var arr = [1,2,3];
var children = arr.map( (n) => <li>item #{n}</li> );
container.content(children);
```
这里将生成3个`<li>`的元素插入到container

下面是支持vnode的方法
* `element.content(vnode | array of vnodes)` \- 设置element的内容为vnode content of the element is replaced by these elements;
* `element.append(vnode | array of vnodes)` \- 添加vnode到element内容的结尾;
* `element.prepend(vnode | array of vnodes)` \- 添加vnode到element内容的开头;
* `element.patch(vnode)` \- 使用vnode更新DOM元素,详情见下面;

### `element.patch(vnode)` \- DOM重复使用

`element.patch(vnode)`函数:

* 使用`attrs`更新DOM的属性,创建新的/删除旧的/修改已有的.
* 对于子节点:
  * 创建新的子节点;
  * 删除旧的子节点;
  * 递归修改子节点.

`patch()`使用如下规则匹配子节点:

* 包含相同的`key`属性;
* 或者包含相同的`id`属性;
* 或者包含相同的`name`属性;
* 或者包含使用相同的`tag`;

更多虚拟dom更新机制可以查看
[虚拟dom](https://vue-js.com/learn-vue/virtualDOM)
[renderer-patch](http://hcysun.me/vue-design/zh/renderer-patch.html)