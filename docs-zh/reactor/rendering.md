
备注:这篇文档有意的与 [ReactJS/Rendering-Elements](https://reactjs.org/docs/rendering-elements.html)组织成相似的结构,以便ReactJS阅读理解

## 渲染元素

Reactor's的虚拟dom是真实dom的一种结构定义,可以使用虚拟dom创建真实的dom元素

```JavaScript
const velement = <h1>Hello, world</h1>;
```
DOM元素包含具体的实现以及响应的数据内容,而Reactor的vdom只是脚本中的一个数组变量,
相对DOM而言,vdom更为轻量,经常将vdom作为element的方法的参数来操作真实dom

## 渲染vdom到DOM

在HTML中声明一个`div`如下
```XML
<div id="root"></div>
```

如上是一个真实DOM节点作为根节点,在html解析后渲染到窗口中
可以使用一个或多个根节点

调用 `element.patch(velement)`渲染虚拟dom到根节点
```JavaScript
const velement = <div id="root"><h1>Hello, world</h1></div>;
document.$("div#root").patch(velement);
```


## 更新节点内容 Updating the Rendered Element

`element.patch(velement)`也可以用来更新已有dom元素


```JavaScript
function tick() {
  const velement = <div id="root">
      <h1>Hello, world!</h1>
      <h2>It is {new Date().toLocaleTimeString()}.</h2>
    </div>;
  document.$("div#root").patch(velement);
  return true; // to keep timer running
}

setInterval(tick,1000);
```


## Element.patch()只更新必要的内容
上面的`patch()`追更新`<h2>`的内容
这也是JSX编程的思想核心,使用JSX表示每个时间点页面的内容,
然后`patch()`自动计算需要更新的DOM内容