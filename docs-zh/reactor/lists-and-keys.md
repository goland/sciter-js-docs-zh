## 列表和keys

可以在JSX的`{}`引用多个jsx列表元素
下面使用`map()`函数生成多个`<li>`,然后插入到JSX表达式中

```JavaScript
const numbers = [1, 2, 3, 4, 5];
const listItems = numbers.map((number) =>
  <li>{number}</li>
);
```
```JavaScript
document.body.content(<ul>{listItems}</ul>);
```
上面的效果类似`Element.content(vnodes)`函数的调用

如果我们更新上面的列表元素,可以通过添加`keys`来提高效率

[keys相关说明](http://hcysun.me/vue-design/zh/renderer-diff.html#key-%E7%9A%84%E4%BD%9C%E7%94%A8)

通常使用列表元素具有唯一性的值作为key

```JavaScript
const todoItems = todos.map( todo =>
  <li key={todo.id} status={todo.status}>
    {todo.text}
  </li>);
```
备注:不建议使用列表的索引作为key,可能会引起渲染错误.

#### Keys必须保障唯一性 Keys Must Only Be Unique Among Siblings

一个数组中的Keys必须保障唯一性

#### Embedding map() into JSX expressions

JSX允许在`{}`嵌入合法的的js表达式,下面在`{}`中使用`map()`方法
```JavaScript
function NumberList(props) {
  const numbers = props.numbers;
  return
    <ul>
     { numbers.map((number) => <li key={number.toString()} />) }
    </ul>;
}
```
