# Reactor vs ReactJS

## `this` 环境变量

* Reactor –  `this`表示真实DOM元素.
* React.JS – `this`表示`React.Component`.没有关联真实的DOM元素.ReactJS使用Refs关联真实DOM,

## `constructor(props,kids,parent)`

`constructor`在每个DOM类创建实例时调用一次,此时的DOM实例还未挂载到真实元素
相等于ReactJS的`componentWillMount()`

## `componentDidMount()` 
ReactJS和Reactor中2者都表示挂载后的回调


## `componentWillUnmount()`
ReactJS和Reactor中2者都表示删除前的回调

## `render(props,kids)`

Reactor中`render()`在以下2种情况调用
1. 将VDOM转换为DOM时,比如从jsx表达式: `<Component prop\="..." />`.创建DOM时,此时标签属性作为`props`参数,子元素作为`kids`参数传递给构造函数.
2. 调用`element.componentUpdate({...})`,自动调用`render()`.

因此Reactor.render()是`ReactJS.constructor()` as `ReactJS.render()`的组合.

## `state.reconciliation = true | false`

`element.state.reconciliation = false`将会组件更新子元素,相等于ReactJS的[**shouldComponentUpdate()**](https://reactjs.org/docs/react-component.html#shouldcomponentupdate)

