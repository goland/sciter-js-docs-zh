
# Reactor是什么

Reactor包含3个功能

* Sciter脚本编译器原生支持[JSX](JSX.md),JSX语法和表达式是Sciter的js语法的一部分
* Sciter原生支持DOM/VDOM编程. `element.patch(jsx-expression)`使用vdom机制更新`element`的内容.

# Reactor不是什么

* Reactor不是外部库和框架,是SciterJs的内置功能,实现了React的编程模型

## Hello World

下面是一个Sciter的Reactor的demo
```JavaScript
document.$(body).patch(<body>Hello, world!</body>);
```

正如预期,执行结果如下,
```XML
<body>
  Hello, world!
</body>
```
这里的`<body>Hello, world!</body>`是一个JSX表达式组成的字面量(字面量:可以看做一个简单值)
备注:这里不需要安装任何JSX相关的编译器(babel),JSX是SciterJS的内置功能

## 如果Reactor包含如上功能...

那就意味着可以使用[ReactJS]的相关功能
JSX, for example, can be used in cases where you need to populate existing DOM. 
比如,使用JSX来操作已有的DOM
比如,使用`element.append(<li>new list item</li>)`来添加一个新元素到列表中

## SciterJS::Reactor 与 Facebook::ReactJS

Sciter's的Rector使用了与ReactJS相似的设计理念
ReactJS是一个js库,Reactor是一个原生实现的类Reactjs。
其中仅仅包含2个功能**JSX**和**element.patch()**
只需要上面2个功能即可在Sciterjs中使用Reactjs的相似编程模型

# 更多阅读

* [JSX](JSX.md) - 基础元素和原理
* [Rendering elements](rendering.md) - element.patch()函数
* [Components](components.md) - functional组件和class组件
* [Component state and lifecycle](component-update.md) - componentUpdate(state)等组件状态与生命周期.
* [Styles, Events and Mounting points](component-styles-events.md) 样式,事件,挂载点
* [Lists and Keys](lists-and-keys.md) 列表和keys
* [Technical details: Reactor vs ReactJS](reactor-vs-reactjs.md) 实现细节对比