
备注:这篇文档有意的与[ReactJS/Components and Props](https://reactjs.org/docs/components-and-props.html)组织成相似的结构,以便ReactJS阅读理解

## Components和Props的本质

`components`组件本质上是js函数
接受输入内容`props`返回虚拟dom,

## Function组件和Class组件

最简单的定义组件方式是使用函数,如下:
```JavaScript
function Welcome(props) {
  return <h1>Hello, {props.name}</h1>;
}
```
如上接受一个`props`参数,返回一个JSX表达式的函数称为函数式组件(Function components)

也可以使用`class`来定义组件,如下:
```JavaScript
class Welcome extends Element {
  constructor(props) { 
    super();
    this.props = props; 
  }
  render() {
    return <h1>Hello, {this.props.name}</h1>;
  }
}
```
上面的两种方式效果一样
`Class`组件包含更多的功能,
通常使用`Function`作为简单使用,`Sciterjs`不支持`Hook`功能


#### 备注:函数组件或者Class组件的名称通常使用大写字母开头.

### 函数式组件和组件的构造函数 Component-functions and component constructors signatures

所有的函数组件和class组件的构造函数结构如下
```
function FunctionComponent(props,kids[,parent]) {} 
```

* *props* 表示属性参数;
* *kids*  表示子节点参数;
* *parent* 表示父节点参数 .     
* *this* 环境变量表示父元素节点,可能是`null`或者一个已存在元素.

```XML
<FunctionComponent mode="start">
   <div>bar</div>
</FunctionComponent>
```
解析后得到的函数参数如下

* *props* - `{ mode: "start" }`
* *kids* - `[ <div>bar</div> ]`

对应的Class组件的构造函数如下
```JavaScript
class ClassComponent extends Element {
  constructor(props,kids,parent) {...} 
}
```

*parent* 接受真实的DOM元素.

## 渲染一个组件

正如渲染JSX的虚拟dom一样
```JavaScript
const velement = <div />;
```
也可以使用自定义的组件
```JavaScript
const velement = <Welcome name="John" />;
```
上面的代码将会解析为对函数组件或者Class组件的构造函数的调用

```JavaScript
function Welcome(props) {
  return <h1>Hello, {props.name}!</h1>;
}

const velement = <Welcome name="Ivan" />;

document.body.content(velement);
```

整个流程总结如下:

1. 调用`element.content(velement)`,传递参数`<Welcome name="Ivan" />`.
2. Sciter调用`Welcome`组件函数,传递参数`{name: "Ivan"}`作为`props`.
3. 定义的`Welcome`组件返回一个JSX表达式 `<h1>Hello, Ivan</h1>`.
4. `element.content()`将虚拟dom`<h1>Hello, Ivan!</h1>`添加到`body`中.

## Components的组合

组件还可以引用其他的组件作为返回值，
因此可以定义一个组件包含所有的子组件，
比如创建一个`App`组件返回多个`Welcome`组件
```JavaScript
function Welcome(props) {
  return <h1>Hello, {props.name}!</h1>;
}

function App() {
  return <body>
       <Welcome name="Ivan" />
       <Welcome name="Olga" />
       <Welcome name="Andrew" />
    </body>;
}

document.body.patch( <App /> );
```

## 抽取组件Components

将一个组件内容拆分为多个子组件称为抽取组件`Extracting Components`

```JavaScript
function Comment(props) {
  return
    <div .comment>
      <div .userinfo>
        <img .avatar src={props.author.avatarUrl} alt={props.author.name} />
        <div .userinfo-name>{props.author.name}</div>
      </div>
      <div .comment-text>{props.text}</div>
      <div .comment-date>{formatDate(props.date)}</div>
    </div>;
}
```
可以将上面的组件拆分为几个组件如下

```JavaScript
function Avatar(props) {
  return <img.avatar
          src={props.user.avatarUrl}
          alt={props.user.name}/>;
}
```

```JavaScript
function UserInfo(props) {
  return <div.userinfo>
      <Avatar user={props.user} />
      <div.userinfo-name>{props.user.name}</div>
    </div>;
}
```

```JavaScript
function Comment(props) {
  return <div.comment>
   <UserInfo user={props.author} />
   <div.comment-text>{props.text}</div>
   <div.comment-date>{formatDate(props.date)}</div>
  </div>;
}
```
