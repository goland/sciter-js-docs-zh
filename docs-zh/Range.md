# class Range

表示文档的一个片段,
可以包含文本节点和元素节点

#### 属性 Properties:  

* `range.isCollapsed:bool` - true if the range is collapsed to one position (start == end)
* `range.commonAncestorContainer:Element` - nearest container element that encloses as start as end positions
* `range.endContainer: Node`
* `range.endOffset: int`
* `range.startContainer:Node` - focusNode/Offset is a caret position.
* `range.startOffset:int`
* `range.start:[node,offser]`
* `range.end:[node,offser]`

#### 方法 Methods:

* `range.setStart(node,offset)`
* `range.setEnd(node,offset)`
* `range.setStartBefore(node)`
* `range.setEndBefore(node)`
* `range.setStartAfter(node)`
* `range.setEndAfter(node)`
* `range.selectNode(node)`
* `range.selectNodeContents(node)`
* `range.getRangeAt(index:uint):Range` - returns Range object at given index.
* `range.selectNodeContent(node)` - selects the node.
* `range.collapse([toStart:bool])` - sets either end or start positions.
* `range.cloneRange():Range` - returns copy of this range object.

#### 特有方法 Methods (Sciter specific):

* `range.applyMark(name | [name1, name2,...])` - apply mark(s) to the range so it can be styles by CSS `::mark(name) {...}`.
* `range.highlight(name | [name1, name2,...])` - synonym of `applyMark`.
* `range.clearMark(name | [name1, name2,...])` - removes mark(s) from the range.
* `range.clearHighlight()` - synonym of `clearMark`
