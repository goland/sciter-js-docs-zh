### Event类 class Event

#### 属性 properties:

* `event.bubbles`
* `event.cancelable`
* `event.currentTarget`
* `event.defaultPrevented`
* `event.eventPhase`
* `event.target`
* `event.type`
* `event.data`
* `event.details`
* `event.keyCode`
* `event.code` 表示按键的`keyCode` "KeyENTER", "KeyF1"...
* `event.altKey` 是否按下`alt`间 true/false
* `event.ctrlKey`
* `event.metaKey`
* `event.shiftKey`
* `event.button`
* `event.buttons`
* `event.clientX`
* `event.clientY`
* `event.screenX`
* `event.screenY`

* `event.deltaX` -  wheel deltas
* `event.deltaY`


#### Sciter特有属性 properties:

* `event.x` - 相对坐标x   `event.currentTarget`
* `event.y` - 相对坐标y  `event.currentTarget`
* `event.source` - 表示事件来源元素. 

#### 方法 methods:

* `event.preventDefault()`
* `event.stopImmediatePropagation()`
* `event.stopPropagation()`


### 通用事件名称 Known Events

#### 鼠标事件 Mouse

* `"mousemove"`
* `"mouseenter"`
* `"mouseleave"`
* `"mouseidle"` - mouse stays not moving in the element, the event triggers tooltip show.
* `"mousetick"` - mouse is pressed for some time in element, periodic event
* `"mousedown"`
* `"mouseup"`
* `"mousewheel"`
* `"mousedragrequest"`
* `"dblclick"` | `"doubleclick"`
* `"tripleclick"`

#### 行为事件 Behaviors

* `"click"`
* `"input"` | `"change"` 
* `"press"` 
* `"changing"` 
* `"submit"` 
* `"reset"`  
* `"expand"`  
* `"collapse"`  
* `"statechange"` 
* `"visualstatechange"` 
* `"disabledstatechange"` 
* `"readonlystatechange"` 

* `"contextmenu"` - 右键菜单 context menu request for the element
* `"contextmenusetup"` - notification to setup context menu, context menu DOM element is event.source
* `"animationend"`
* `"animationstart"` 
* `"animationloop"` 
* `"mediachange"` 
* `"contentchange"` 
* `"inputlangchange"` 
* `"pastehtml"` 
* `"pastetext"` 
* `"pasteimage"` 
* `"popuprequest"`  
* `"popupready"`    
* `"popupdismissing"` 
* `"popupdismissed"`  

* `"tooltiprequest"` 

#### 焦点事件 Focus

* `"focusin"`
* `"focusout"` 
* `"focus"` 
* `"blur"` 

#### 键盘事件 Keyboard

* `"keydown"`
* `"keyup"`  
* `"keypress"`
* `"compositionstart"`
* `"compositionend"`

#### 滚动事件 Scroll

* `"scroll"`
* `"scrollanimationstart"` 
* `"scrollanimationend"` 

#### 生命周期 Document lifecycle

关闭 Closing:

* `"close"` | `"unload"` - 文档关闭后即将删除时回调 document is closed and about to be deleted soon.
* `"beforeunload"` - 脚本即将关闭 document is about to be unloaded, script namespace is still operational.
* `"closerequest"` - first phase of document closure, it can be rejected at this point by calling `event.preventDefault()`. 

加载 Loading:

* `"parsed"` - 文档解析完成,脚本未运行 document just got a DOM structure, scripts are not run yet. This event can be handled by document container only (window or frame). 
* `"ready"` | `"DOMContentLoaded"` - 文档加载完成,脚本加载完成 document loaded, DOM is parsed, scripts are loaded and run.
* `"complete"` - 文档加载完成,脚本已执行 document loaded in full scripts wer run, all resources defined in HTML are loaded.


#### 元素状态变化 Element's state change

* `sizechange` - 元素大小变化 change of element dimensions, use `element.onsizechange = function() {}` to setup the event handler;
* `visibilitychange` - 元素可见状态变化 change of element visibility status,  use `element.onvisibilitychange = function() {}` to setup the event handler;

#### 图片事件 Image 

* `"load"`
* `"error"`

#### 打印页面事件 Pager (print preview)

* `"paginationstart"`
* `"paginationpage"` 
* `"paginationend"` 

#### 拖拽事件 Drag-n-drop

* `"drag"`
* `"dragenter"` 
* `"dragleave"` 
* `"drop"` 
* `"dragcancel"` 
* `"dragaccept"` 
* `"willacceptdrop"`

#### 视频事件 Video 

* `"play"`
* `"ended"`

* `"videocoordinate"`
* `"videoframeready"`

## 事件流程 MISC

- [Event handling in Sciter](https://sciter.com/event-handling-in-sciter/)
