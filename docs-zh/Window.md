# class Window

Window的实例表示桌面窗口,是整个渲染窗口的根节点
`Window.this`关联到Window实例,表示HTML内容加载的窗口.

## 构造函数 constructor:

* `new Window {params}`

  `params`是一个对象,可以包含以下属性:

  * `params.type` -可选,窗口类型:

    * `Window.POPUP_WINDOW` - 弹窗
    * `Window.TOOL_WINDOW`  - 工具窗口
    * `Window.CHILD_WINDOW` - 子窗口
    * `Window.FRAME_WINDOW` - 默认窗口类型 default window type
    * `Window.DIALOG_WINDOW` - 对话窗口

  * `params.parent` : Window - 可选,窗口的父窗口.
  * `params.caption` : string - 可选,窗口的标题.
  * `params.x` : integer - 可选,窗口在屏幕的位置水平坐标(左上角?);
  * `params.y` : integer - 可选,窗口在屏幕的位置垂直坐标(左上角?);
  * `params.width` : integer - 窗口宽度;
  * `params.height` : integer - 窗口高度 ;
  * `params.client` : true | false - if `true` then x,y,w,h are coordinates of desired window client box on the screen;
  * `params.alignment` : 1..9 - 可选,对齐方式,optional, alignment of the window on monitor, if -1..-9 and parent is provided then it aligns the window against parent window.
  * `params.screen` : integer - 可选,显示器的序号,多个显示器时使用.
  * `params.state` : - 可选,窗口状态:

    * `Window.WINDOW_HIDDEN`
    * `Window.WINDOW_SHOWN` - default type
    * `Window.WINDOW_MAXIMIZED`
    * `Window.WINDOW_MINIMIZED`
    * `Window.WINDOW_FULL_SCREEN`

  * `params.url` : string -可选,html内容.
  * `params.parameters` : array | string | object, ... - 可选,传递的参数.

## 属性 properties:
 
  * `window.state` - 可读写:
    * `Window.WINDOW_SHOWN`
    * `Window.WINDOW_MINIMIZED`
    * `Window.WINDOW_MAXIMIZED`
    * `Window.WINDOW_FULL_SCREEN`
    * `Window.WINDOW_HIDDEN`
  * `window.graphicsBackend` - 只读,表示渲染方式: "direct2d", "Skia/OpenGL", etc. 
  * `window.minSize = [w,h]` - 读写,窗口缩放最小宽高. 
  * `window.maxSize = [w,h]` - 读写,窗口缩放最大宽高.
  * `window.blurBehind = "none" | "auto" | "dark" | "ultra-dark" | "light" | "ultra-light"` - 失去焦点时的特效.
  * `window.isActive` - 只读,表示窗口是否激活输入 read-only, boolean, reports if window has input focus.
  * `window.isResizable` - 读写,是否可以缩放 read/write, boolean, true if window can be resized by the user.
  * `window.isMaximizable` -读写,是否可放大  read/write, boolean, true if window can be maximized by the user.
  * `window.isMinimizable` -读写,是否可缩小 read/write, boolean, true if window can be minimized by the user.
  * `window.isTopmost` - 读写,是否最上层 read/write, boolean, true if window is topmost at z-order.
  * `window.isEnabled` - 读写,是否可接受输入 read/write, boolean, true if the window is allowed to accept user's input.
  * `window.aspectRatio` - 读写,表示缩放时的微调大小 read/write, float, width to height ratio to keep on window resizes.
  * `window.eventRoot = element | null` - if set by element, short circuits all UI events to that element and its children as if the window contains only that element. Used in lightbox dialog scenarios (see: samples.sciter/lightbox-dialog).
  * `window.focus` - 读写,是否获得焦点 read/write, DOM element in focus.
  * `window.parent` - 只读,父窗口 read-only, Window | null - parent window of this one.
  * `window.document` -只读,文档根节点 read-only, Document - root document of the window.

## 方法 methods:

  * `window.box(boxPart,boxOf[,"screen"]):[...]` 返回窗口坐标信息 reports geometry of the window, where:
  
    _boxPart_ 表示返回的内容,defines what part of the box to return, is one of:
  
    * `"xywh"` or `"rectw"`  - [x,y,w,h], array, position and dimension of the rectangle.
    * `"rect"` - [x1,y1,x2,y2], array, positions of origin and corner of the rectangle.
    * `"position"` - [x,y], array, position of the rectangle.
    * `"dimension"` - [w,h], array, dimension of the rectangle.
    * `"left"`,`"top"`,`"right"`,`"bottom"`,`"width"`,`"height"` - individual integers.

    _boxOf_ is one of:

    * `"border"` - 边框 border area of the window that includes window caption and borders arouns client area;
    * `"client"` - 客户区 client (content) area of the window.  

    If _"screen"_ parameter is provided then function returns coordinates relative to screen. 

    Note: the function returns values in screen pixels.
  
  * `window.move(x,y [,width, height [, "client" ]])` - 移动/缩放窗口 move/size window;
  * `window.close([value])` - 请求关闭,对话窗口使用 request to close the window, the value is a return value used in modal dialogs;
  * `window.selectFile(...)` - 打开选择文件窗口 file open/save dialog, TBD;
  * `window.selectFolder(...)` - 打开选择目录窗口 folder open dialog, TBD;
  * `window.mediaVar(varname[,value])` - 设置/获取css中media变量信息 gets/sets media variable that can be used in CSS as `@media varname {...}`
  * `window.mediaVars([values:object])` -设置/获取media变量 gets/sets media variables. 
  * `window.addEventHandler("eventname", handler)` -添加事件回调 subscribe to window related events 
  * `window.on("eventname", handler)` - 添加事件回调 subscribe to window related events 
  * `window.off("eventname" | handler)` - 去掉事件回调 unsubscribe event handler either by name, namespace or handler reference  
  * `window.xcall(name:string [, arg0, ..., argN]): any`
    调用窗口关联的原生的行为`native behaviors`,
    Interaction with native behaviors attached to the window. `Window.this.xcall("foo")` will end up in [`handle_scripting_call()`](https://github.com/c-smile/sciter-js-sdk/blob/main/include/sciter-x-behavior.h#L749) of native behavior attached to the window using [SciterWindowAttachEventHandler](https://github.com/c-smile/sciter-js-sdk/blob/main/include/sciter-x-behavior.h#L898) API.

  * `window.trayIcon({image: Graphics.Image, text: string})` - show tray icon with the image and tooltip text.

    Tray icon will generate ["trayiconclick"](#trayiconclick) event on user clicks.

  * `window.trayIcon("remove")` - remove tray icon.
  * `window.trayIcon("place") : [x,y,w,h]` - reports location of the icon on desktop, coordinates are in screen pixels.
  * `window.screenBox(what [, boxPart])` - 返回显示器的信息 reports geometry of monitor this window is on. 

    _what_ defines what information to return, is one of:

    * `"frame"` - physical position and size of the monitor in screen pixels projected on desktop.
    * `"workarea"` - physical position and size of work area on the monitor ( frame minus taskbar )
    * `"device"` - string, name of the monitor.
    * `"isPrimary"` - boolean, true is that is primary monitor.
    * `"snapshot"` - Graphics.Image, returns snapshot (screenshot) of the monitor.

    _boxPart_ defines what part of the box to return, is one of:

    * `"xywh"` or `"rectw"`  - [x,y,w,h], array, position and dimension of the rectangle.
    * `"rect"` - [x1,y1,x2,y2], array, positions of origin and corner of the rectangle.
    * `"position"` - [x,y], array, position of the rectangle.
    * `"dimension"` - [w,h], array, dimension of the rectangle.
    * `"left"`,`"top"`,`"right"`,`"bottom"`,`"width"`,`"height"` - individual integers.

  * `Window.this.modal(JSX) : any` - 显示提示信息窗口 shows message box: `<info>..</info>`, `<alert>..</alert>`, `<error>..</error>`, `<question>..</question>`.
  * `Window.this.modal({params}) : any` - shows new window as dialog, for params see `new Window({params})` above. The function returns window close value of `Window.this.close(valToReturn)` call inside the window. 

  * `Window.this.performDrag(data:object, mode: "copy" | "move", dragIcon: Image | Element[, dragIconXoff:int, dragIconYoff:int] ): null | "copy" | "move"` - performs drag-and-drop using system D&D mechanism.

    `data` is an object that may contain one or several fields: 
    * `text: string` - plain text data;
    * `html: string` - HTML data; 
    * `file : [path1,path2,...] | path0` - single or multiple file names;
    * `json`: any - any data that can be JSON.stringify'ed;

  * #### `Window.this.focusable(dir [,reference:element]): element`
    
    The functions allows to enumerate elements in tab order. _dir_ there is one of:

    * "next" - next focusable element after the _reference_;
    * "prior" - previous focusable element after the _reference_;
    * "first" - first focusable DOM element on the window;
    * "last" - last focusable DOM element on the window;

    You can assign found element to `window.focus = element` set focus on it.

    
## 类的方法和属性 class methods and properties:

  * `Window.this` 
    Window实例对象 
    instance of Window class - this window reference;

  * `Window.screenBox(monitor:integer, what, boxPart)` 
    显示信息获取
    reports geometry and information of the given monitor. For _what_ and _boxPart_ parameters see window.screenBox() method above.

  * `Window.elementAt(screenX,screenY):Element` 
    返回坐标处的DOM原生
    returns DOM element under screenX/screenY position. 
    Note: this method may return DOM element belonging to any Sciter window in current process. 

  * `Window.ticks():milliseconds`
    返回内部计时器时间
    returns value of internal timer. 

  * `Window.post( ge: Event )`
    调用事件
    posts global event *ge* to all windows in current process.   


## 事件函数 events

Use `Window.this.on("eventname", handler)` to subscribe to these events: 
  支持的事件如下
  * `"statechange"` - `window.state` flag have changed. 
  * `"resolutionchange"` - after window moved to another monitor with different resolution, or user have changed screen resolution. 
  * `"mediachange"` - one or several CSS media variables have changed. 
  * `"activate"` - the window was deactivated (evt.reason == 0) or got focus (evt.reason > 0)
  * `"replacementstart"` 
  * `"replacementend"` - user start/end moving or resizing window chrome.
  * `"move"` - user moved the window.
  * `"size"` - user changed size of the window.
  * <a id="trayiconclick"></a>`"trayiconclick"` - click on tray icon.

