# class Node 
`Node`是一个作为树节点抽象类,表示树的概念
[Element](Element.md),[Text](#class-text-extends-class-node),[Comment](#class-comment-extends-class-node)是`Node`的子类实现
这3个类包含`Node`的属性和方法

#### 属性 properties:

* `node.nodeName`
* `node.nodeType`
* `node.nodeValue`
* `node.nodeIndex`
* `node.firstChild`
* `node.lastChild`
* `node.nextSibling`
* `node.previousSibling`
* `node.ownerDocument`
* `node.parentNode`
* `node.parentElement`
* `node.parentWindow` -节点所在的window实例;
* `node.textContent`

#### 方法 methods:

* `node.cloneNode()`
* `node.contains()`
* `node.compareDocumentPosition()` - 未实现
* `node.getRootNode()`
* `node.hasChildNodes()`
* `node.isEqualNode()`
* `node.isSameNode()`
* `node.remove()`


# Text节点类

表示html中的文本节点

#### 扩展属性 properties:

* `text.data` read/write
* `text.length` read-only
* `text.wholeText` read-only

# Comment节点类

表示html中的注释节点

#### 扩展属性 properties:

* `comment.data` read/write
* `comment.length` read-only
