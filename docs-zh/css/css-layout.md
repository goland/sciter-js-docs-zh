# CSS Layout

Sciter use different layout engine than what is used in browsers.
Sciter的布局引擎

## Flow/Flex

Flow in Sciter is like Flexbox in browsers, but it use different properties
Flow类似浏览器的Flex

`flow : vertical|vertical-flow|horizontal|horizontal-flow`

- [Sciter flex vs flexbox](https://terrainformatica.com/w3/flex-layout/flex-vs-flexbox.htm)
- [flex layout](https://sciter.com/docs/flex-flow/flex-layout.htm)

## Flow: Grid


```CSS
flow: "1 2"     | 1 | 2 |
      "1 3";    |   | 3 |
```

## Align

- `horizontal-align` : `center|left|right`
- `vertical-align` : `middle|top|bottom`
