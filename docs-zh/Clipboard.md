# Clipboard namespace

支持剪贴板的操作

## Methods:

* #### `Clipboard.read()` 
  获取剪贴板的内容对象格式
  returns [object containing clipboard data](#clipboard-data)

* #### `Clipboard.readText()` 
  获取剪贴板的内容字符串形式
  returns either string or undefined if clipboard is doe not contain textual data.

* #### `Clipboard.write(data)` 
  添加数据到剪贴板
  Puts [data](#clipboard-data) into clipboard.  

* #### `Clipboard.writeText(string)` 
  添加字符串到剪贴板
  Puts the string into clipboard.  

## Clipboard Data Object
  剪贴板的数据对象格式
Clipboard data object is a plain JS object that may contain following properties:

* `text`: string - text;
* `html`: string - html fragment;
* `json`: value - arbitrary JSON data;
* `file`: [path0,path1, ...] - list of file paths;
* `link`: { caption: string, url: string} - link to some file / location;
* `image`: Graphics.Image - image object;

