# class Element.Style

`Element.Style`的实例表示DOM元素的CSS属性组成的列表

可以通过`element.style`获得

#### 属性 Properties:  

获取/修改属性
```JavaScript
var bgColor = element.style.backgroundColor;
```
```JavaScript
var bgColor = element.style["background-color"];
```

#### 方法 Methods:

* `element.style.getPropertyValue(name)`
* `element.style.setProperty(name, value [,important])`
* `element.style.removeProperty(name)`
* `element.style.colorOf(name)` 获取的颜色信息 reports color property _name_ as instance of [`Graphics.Color`](Graphics/Graphics.Color.md) class.
* `element.style.variables() : {}` reports CSS variables seen by the element, reports the set as {name:value, ...} map.
* `element.style.variables({name:value,...})` sets CSS variables on the element.
* `element.style.setCursor(image | null,hotspotX,hotspotY)` set/reset element's cursor by image.

