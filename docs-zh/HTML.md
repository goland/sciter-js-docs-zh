# HTML

Sciter对HTML语法进行了扩展,支持以下缩写形式
| Sciter | Regular HTML |
| ------ | ------------ |
| `<input #id />`   | `<input id="id" />`
| `<input .class />`| `<input class="class" />`
| `<input\|text />` | `<input type="text" />`
| `<input(name) />` | `<input name="name" />`


## 标签元素 Elements

Sciter扩展的标签元素如下,扩展了事件行为 [behavior](behaviors/README.md)
| Element | Description |
| ------- | ----------- |
| `<popup>` | popup element (preferred to be placed in `<head>`)
| [`<menu .context>`](behaviors/behavior-menu.md)  | [context-menu styled](CSS/css-sciter.md) element
| [`<plaintext>`](behaviors/behavior-plaintext.md) | Multiline text editor
| [`<htmlarea>`](behaviors/behavior-richtext.md) | WYSIWYG/richtext/html editor
| [`<frameset>`](behaviors/behavior-frame-set.md) | child elements to be resizable window blocks
| [`<select\|tree>`](behaviors/behavior-tree-view.md) | Tree-list select element, one of [behavior select](behaviors/README.md) types
| `<include src=""/>` | Inserts HTML file in place.


## 属性 Attributes

| Attribute  | Description |
| ---------  | ----------- |
| `spellcheck` | true/false enable or disable spellcheck
| `selectable` | allow content selection (behavior)
| `novalue`    | synonym of `placeholder`
| `role="window-caption"` | Allow to drag the window through this element


## 窗口属性 Window Attributes

Window (`<html>`) specific attributes
`<html>`标签的属性

| Attribute | Description |
| --------- | ----------- |
| `window-frame` | `default\|standard\|solid\|solid-with-shadow\|extended\|transparent` define window type
| `window-icon`  | Window icon URL
| `window-title` | Window title
| `window-width` | initial width of the window
| `window-height`| initial height of the window
| `window-resizable`  | `true\|false\|LENGTH-UNIT` i.e. `10px` counted from window frame inwards
| `window-blurbehind` | `auto\|dark\|light\|ultra-dark\|ultra-light` translucent effect.
| `lang` | ISO 639-1 value, define dictionary for spellcheck, Date...


## 其他 Misc

- Sciter allow the use of the custom element tags, make sure to give it a default style.
- You can show popup with [`Element.popupAt`](Element.md#popup).
- Attribute events (onclick..) are not supported. (unless you implements method for it to work).
- String `&platform-cmd-mod;` is replaced with `Ctrl/CMD...`
- [List of input elements](https://sciter.com/developers/for-web-programmers/input-elements-map/)
