# module `@sys`
```js
import * as sys from "@sys";
```
包含了系统相关的操作方法

所有的方法与Nodejs保持相似的操作方法

```JavaScript
const p = new sys.Pipe();

async function connect() {
  await p.connect('fooapp');
  console.log(`Connected to ${p.getpeername()}`);   
  ...
}
```

`sys`是基于[libuv](https://github.com/libuv/libuv)构建
参考自项目[txiki project](https://github.com/saghul/txiki.js/)

## `sys.fs`文件操作.

#### 文件函数 functions

* `fs.open(): promise(File)`
* `fs.stat(): promise(stat)`
* `fs.$stat(): stat` - sync version of the above;
* `fs.lstat(): promise(stat)`
* `fs.$lstat(): stat` - sync version of the above;
* `fs.realpath()`
* `fs.unlink()`
* `fs.rename()`
* `fs.mkdtemp()`
* `fs.mkstemp()`
* `fs.rmdir()`
* `fs.$mkdir(path)` - creates folder (synchronous).
* `fs.copyfile()`
* `fs.readdir()`
* `fs.$readdir(): filelist` - reads folder content synchronously
* `fs.readfile() : promise` - async fileread;
* `fs.$readfile() : ArrayBuffer` - synchronous version of the above; 
* [`fs.watch()`](sys.fs/watch.md)
* [`fs.splitpath()`](sys.fs/splitpath.md)

#### 类 classes

### fs.File class - represents file. 

* `file.read()`
* `file.write()`
* `file.close()`
* `file.fileno()`
* `file.stat()`
* `file.path`

### fs.Dir class - directory visitor 

* `dir.close()`
* `dir.path`
* `dir.next()`
* `[async iterator]`

## 网络函数 Network functions.

### TCP socket class 

* `socket.close()`
* `socket.read()`
* `socket.write()`
* `socket.shutdown()`
* `socket.fileno()`
* `socket.listen()`
* `socket.accept()`
* `socket.getsockname()`
* `socket.getpeername()`
* `socket.connect()`
* `socket.bind()`

### UDP socket class 

* `socket.close()`
* `socket.recv()`
* `socket.send()`
* `socket.fileno()`
* `socket.getsockname()`
* `socket.getpeername()`
* `socket.connect()`
* `socket.bind()`

### Pipe - IPC mostly  

* `socket.close()`
* `socket.read()`
* `socket.write()`
* `socket.fileno()`
* `socket.listen()`
* `socket.accept()`
* `socket.getsockname()`
* `socket.getpeername()`
* `socket.connect()`
* `socket.bind()`

### TTY primitives 

* `tty.close()`
* `tty.read()`
* `tty.write()`
* `tty.fileno()`
* `tty.setMode()`
* `tty.getWinSize()`

## 进程相关 `sys.spawn()` - running processes with stdin/strout/stderr redirection.

* `process.kill()`
* `process.wait()`
* `process.pid`
* `process.stdin`
* `process.stdout`
* `process.stderr`

## 其他的函数 `sys.****` - miscellaneous functions.

* `sys.hrtime()`
* `sys.gettimeofday()`
* `sys.uname()`
* `sys.isatty()`
* `sys.environ()`
* `sys.getenv()`
* `sys.setenv()`
* `sys.unsetenv()`
* `sys.cwd()`
* `sys.homedir()`
* `sys.tmpdir()`
* `sys.exepath()`
* `sys.random()`




