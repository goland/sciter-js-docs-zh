# module storage
```js
import * as Storage from "@storage"; 
```

`storage`模块实现了数据的持久化存储,

[Introduction简介](./introduction.md) 使用方法 and usage manual.

[Architecture架构](./architecture.md) 实现细节explained.

文档:

  * class [Storage](./Storage.md);
  * class [Storage.Index](./Storage.Index.md);



