# module `@env`
```js
import * as env from "@env";
```
表示当前的操作系统以及设备信息
#### 常量 constants:

* `env.OS` - 操作系统 OS identification name, for example `"Windows-8.1"`
* `env.PLATFORM` - 运行环境 OS/platform generic name: `"Windows"`, `"OSX"`, `"Linux"`, `"Android"`, etc.
* `env.DEVICE` - 运行设备 device type: `"desktop"`, `"mobile"`

#### functions:

* `env.language()` - 获取语言信息 returns two-letter language abbreviation of user's default language, for example `"en"` for English.
* `env.country()` - 获取国家信息 returns two-letter country abbreviation, for example `"CA"` for Canada.  
* `env.userName()` - 获取用户名信息 returns current user name. 
* `env.machineName()` - 获取设备网络信息 machine network name.  
* `env.domainName()` - 获取设备域名信息 machine network domain.
* `env.launch(path)` - 自动启动浏览器 method to open documents and start applications;
    Example: `env.launch("https://sciter.com")` will open default browser with that url.
* `env.home([relpath]): string` - 返回`sciter.dll`的绝对路径 converts relative path to absolute path using location of sciter.dll as a base. 
* `env.path(name): string` - 返回绝对路径 returns location of well known folders on user machine, name is one of: 
  * "USER_HOME"
  * "SYSTEM"
  * "SYSTEM_BIN"
  * "PROGRAM_FILES"
  * "USER_APPDATA"
  * "COMMON_APPDATA"
  * "USER_DOCUMENTS"
  * "COMMON_DOCUMENTS"
  * "DOWNLOADS"
* `env.variable(name:string [,toset:string | null]): string` - 设置和读取环境变量 TBD
* `env.variables(): object` - 读取环境变量 TBD
* `env.exec()` 执行命令 execute comma-separated arguments. `exec("scapp", "main.html")`


